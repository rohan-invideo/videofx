//
//  VideoFX.h
//  VideoFX
//
//  Created by Wesley Dos Santos on 05/06/19.
//  Copyright © 2019 Filmr. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for VideoFX.
FOUNDATION_EXPORT double VideoFXVersionNumber;

//! Project version string for VideoFX.
FOUNDATION_EXPORT const unsigned char VideoFXVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <VideoFX/PublicHeader.h>
