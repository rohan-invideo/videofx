// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
  name: "VideoFX",
  platforms: [.iOS(.v13)],
  products: [
    .library(
      name: "VideoFX",
      targets: ["VideoFX", "VideoFXPackage"]),
  ],
  dependencies: [
  ],
  targets: [
    .target(name: "VideoFX"),
    .binaryTarget(name: "VideoFXPackage", path: "VideoFX.xcframework"),
  ]
)
