Pod::Spec.new do |spec|
  spec.name         = "VideoFX"
  spec.version      = "0.0.1"
  spec.summary      = "VideoFX made by Filmr"
  spec.homepage     = "https://bitbucket.org/rohan-invideo/videofx/"
  spec.source       = { :git => "Not Published", :tag => "Cocoapods/#{spec.name}/#{spec.version}" }
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author       = ""
  spec.ios.deployment_target = '13.0'
  spec.requires_arc = true

  spec.vendored_frameworks  = "VideoFX.xcframework"
  spec.module_name          = "VideoFX"
end
